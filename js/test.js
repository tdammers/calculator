"use strict";

/**
 * A minimal test "framework" module.
 */

var Test = (function() {
    /**
     * Takes an arbitrary number of Testable objects, and runs all the test
     * cases found in them.
     *
     * Prints the results to the console.
     *
     * The Testable interface:
     *
     * .testCases - an array of objects, each having a 'name' key pointing to a
     *              string name for the test case, and a 'run' key pointing to
     *              a function that represents the test case itself. The 'run'
     *              function should throw on failure, exit cleanly on success.
     */
    var run = function() {
        var testables = arguments;
        var testCases = [];
        var i;
        var testCase;
        var success = 0, failure = 0;
        for (i = 0; i < testables.length; ++i) {
            testCases = Array.concat(testCases, testables[i].testCases || []);
        }
        for (i = 0; i < testCases.length; ++i) {
            testCase = testCases[i];
            try {
                testCase.run();
                console.log(testCase.name, "OK");
                success++;
            }
            catch (e) {
                console.log(testCase.name, "FAIL");
                console.log(e);
                failure++;
            }
        }
        console.log(success, "tests succeeded,", failure, "tests failed");
    };

    return {
        run: run
    };
})();

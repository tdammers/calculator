"use strict";

/**
 * The UI module.
 *
 * Calling `UI.init(document);` should take care of all the required rigging.
 */
var UI = (function() {
    /**
     * Dispatch a key event into a calculator state.
     *
     * Because keypresses can be reported as character codes or key codes,
     * depending on the key in question, this function takes both as arguments,
     * picking whichever is mapped to a valid command.
     *
     * Returns true if a command has been dispatched, false if not.
     */
    var dispatchKey = function(state, charCode, keyCode) {
        var keyMap = {
            '0': '0',
            '1': '1',
            '2': '2',
            '3': '3',
            '4': '4',
            '5': '5',
            '6': '6',
            '7': '7',
            '8': '8',
            '9': '9',
            '.': '.',
            '+': 'add',
            '-': 'sub',
            '*': 'mul',
            'x': 'mul',
            '/': 'div',
            ':': 'div',
            '^': 'exp',
            'e': 'exp',
            '=': 'go',
            'c': 'clear',
            'C': 'reset'
        };

        var keyCodeMap = {
            8: 'clear',
            13: 'go',
            27: 'clear'
        };

        var command = keyCodeMap[keyCode] || keyMap[String.fromCharCode(charCode)];
        if (command) {
            CalcEngine.dispatchCommand(state, command);
            return true;
        }
        else {
            return false;
        }
    };

    /**
     * Initialize the UI and create calculators as needed.
     *
     * The rootElem parameter is the top-level element within which calculators
     * should be detected and attached, as well as the element to which a
     * keypress event handler is attached.
     */
    var init = function(rootElem) {
        var calcElems = rootElem.getElementsByClassName('js-calculator');

        // Because there can be several calculators within a root element, we
        // keep track of the "selected" one, dispatching keyboard events to
        // only this particular calculator. We need to track both the
        // calculator state and the corresponding DOM element.
        var selCalcState = null;
        var selCalcElem = null;

        /**
         * Updates the virtual calculator display and button highlights to
         * reflect the calculator state.
         */
        var updateDisplay = function(state, calcElem) {
            var displayElems = calcElem.getElementsByClassName('js-display');
            var buttonElems = calcElem.getElementsByTagName('button');
            F.forEach(displayElems, function(displayElem) {
                displayElem.textContent = CalcEngine.effectiveDisplay(state);
            });
            F.forEach(buttonElems, function(buttonElem) {
                if (buttonElem.value === state.operation) {
                    buttonElem.className = 'active';
                }
                else {
                    buttonElem.className = '';
                }
            });
        };

        /**
         * Updates the active-calculator selection.
         */
        var selectCalc = function(state, calcElem) {
            selCalcState = state;
            selCalcElem = calcElem;
        };

        // Create calculators, attach buttons, initialize display
        F.forEach(calcElems, function(calcElem) {
            var state = CalcEngine.init();
            var buttonElems = calcElem.getElementsByTagName('button');

            if (selCalcState === null) {
                selectCalc(state, calcElem);
            }
            updateDisplay(state, calcElem);
            F.forEach(buttonElems, function(buttonElem) {
                var val = buttonElem.value;
                buttonElem.onclick = function(e) {
                    CalcEngine.dispatchCommand(state, val);
                    updateDisplay(state, calcElem);
                    selectCalc(state, calcElem);
                    e.preventDefault;
                };
            });
            calcElem.onclick = function() {
                selectCalc(state, calcElem);
            };
        });

        // Attach key handlers
        rootElem.onkeypress = function(e) {
            if (selCalcState) {
                console.log(e);
                if (dispatchKey(selCalcState, e.charCode, e.keyCode)) {
                    updateDisplay(selCalcState, selCalcElem);
                    e.preventDefault();
                }
            }
            else {
                console.log("No selected calculator");
            }
        };
    };

    return {
        init: init
    };
})();

"use strict";

/**
 * Throw an error if `condition` is falsy.
 */
var assert = function(condition) {
    if (!condition) {
        throw new Error('Assertion failed');
    }
};

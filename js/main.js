"use strict";

(function(global) {
    // Just run all the tests on startup - they run fast, and having them in
    // the console is convenient, lacking a proper testing system ("no
    // third-party libraries" rule)
    Test.run(CalcEngine, F);
    UI.init(document);
})(this);

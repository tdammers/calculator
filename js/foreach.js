"use strict";

var foreach = function(iteree, fn) {
    var i = 0;
    var result = [];
    for (i = 0; i < iteree.length; ++i) {
        result.push(fn(iteree[i], i, iteree));
    }
    return result;
};

"use strict";

/**
 * The calculator engine module.
 *
 * This module is designed to keep state and functionality separate. Unlike the
 * typical OOP approach where the mutators would be members of the state object
 * itself, we're providing an immutable object that contains some state update
 * functions, and have them act upon a mutable but dumb data object.
 *
 * Ideally, the mutator functions wouldn't actually mutate the state in-place,
 * but return a modified copy, thus being pure; however, writing the required
 * helper functions (assoc, dissoc, modify, ...) would blow the project's
 * scope, and using third-party libraries like ramda.js is not allowed, so we
 * settle on in-place mutation of the `state` variable.
 *
 * This approach has a few advantages:
 * - No prototype chain method lookups. We won't need prototype-based
 *   polymorphism here, so there's no need to have the runtime jump through any
 *   extra hoops, which should make the code slightly faster, at least in
 *   theory.
 * - State can easily be serialized/deserialized without any additional
 *   processing. This means that we could, for example, store the calculator
 *   state in LocalStorage, send it over AJAX, put it in DOM elements, etc.,
 *   simply calling JSON.stringify() on it to serialize and JSON.parse() to
 *   deserialize.
 * - Console logging produces more useful output.
 * - Operations can be tested more easily; we can fabricate mock states and
 *   inspect the modified state directly, and independently from the operations
 *   themselves.
 * - Mutation is limited to the state object itself, so the code is easy to
 *   reason about. Other than the state mutations, there are no side effects.
 */
var CalcEngine = (function(){
    /**
     * Creates a new calculator state.
     */
    var init = function() {
        return {
            accum: 0, // holds the result of the calculation so far
            operand: null, // operand for the next calculation step
            operation: null, // operation for the next calculation step
            inputBuffer: '' // input entered so far
        };
    };

    /**
     * Dispatchs a command (given as a string) and executes it.
     *
     * Valid command strings are:
     * - any digit ('0' through '9')
     * - '.' to add a decimal point
     * - 'add', 'sub', 'mul', 'div', 'exp' for arithmetic operators
     * - 'neg' for flipping the sign on the current operand or input buffer
     * - 'clear' to clear the current operand and input buffer
     * - 'reset' to reset the entire calculator state
     * - 'go' to execute the currently entered operation
     */
    var dispatchCommand = function(state, cmd) {
        switch (cmd) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                pushDigit(state, cmd);
                break;
            case '.':
                pushDigit(state, '.');
                break;
            case 'add':
            case 'sub':
            case 'mul':
            case 'div':
            case 'exp':
                pushOperation(state, cmd);
                break;
            case 'neg':
                flipSign(state);
                break;
            case 'clear':
                clear(state);
                break;
            case 'reset':
                reset(state);
                break;
            case 'go':
                go(state);
                break;
        }
    };

    /**
     * Push a digit or decimal point onto the current input buffer.
     */
    var pushDigit = function(state, digit) {
        switch (digit) {
            // 0 needs special treatment, because leading zeroes are ignored.
            case '0':
                if (state.inputBuffer !== '') {
                    state.inputBuffer += digit;
                }
                break;
            // Decimal point needs special treatment, because we can only have
            // one of them in the input.
            case '.':
                if (state.inputBuffer === '') {
                    state.inputBuffer = '0.';
                }
                else if (!(/\./.test(state.inputBuffer))) {
                    state.inputBuffer += digit;
                }
                break;
            default:
                // We only accept digits 1 through 9 at this point.
                if (/^[1-9]$/.test(digit)) {
                    state.inputBuffer += digit;
                }
                break;
        }
        state.operand = null;
    };

    /**
     * Lookup table for arithmetic operators
     */
    var operations = {
        add: function(a, b) { return a + b; },
        sub: function(a, b) { return a - b; },
        mul: function(a, b) { return a * b; },
        div: function(a, b) { return a / b; },
        exp: function(a, b) { return Math.pow(a,b); }
    };

    /**
     * Push an operation to the calculator state. The next call to go() will
     * use this operation.
     */
    var pushOperation = function(state, operation) {
        if (state.inputBuffer) {
            go(state);
        }
        state.operation = operation;
    };

    /**
     * Flip the sign on the input buffer or operand.
     */
    var flipSign = function(state) {
        if (state.inputBuffer) {
            state.inputBuffer = String(-Number(state.inputBuffer));
        }
        else {
            state.accum = -state.accum;
        }
    };

    /**
     * Execute the entered operation.
     */
    var go = function(state) {
        var operation = operations[state.operation] || function(a, b) { return b; };
        if (state.inputBuffer) {
            state.operand = Number(state.inputBuffer);
            state.inputBuffer = '';
        }
        state.accum = operation(state.accum, state.operand || 0);
    };

    /**
     * Reset the operand and input buffer.
     */
    var clear = function(state) {
        state.operand = null;
        state.inputBuffer = '';
    };

    /**
     * Reset the entire calculator state.
     */
    var reset = function(state) {
        state.accum = 0;
        state.operand = null;
        state.operation = null;
        state.inputBuffer = '';
    };

    /**
     * Get the effective value to print to a display. Depending on the
     * calculator state, this will be the value of the input buffer,
     * accumulator, or "0".
     */
    var effectiveDisplay = function(state) {
        return state.inputBuffer || String(state.accum) || "0";
    };

    /**
     * Testable interface: unit tests for this module.
     */
    var testCases = [
        {
            name: "blank state",
            run: function() {
                var state = CalcEngine.init();
                assert(CalcEngine.effectiveDisplay(state) === "0");
            }
        },
        {
            name: "10 + 5 = -> 15",
            run: function() {
                var state = CalcEngine.init();
                CalcEngine.pushDigit(state, 1);
                CalcEngine.pushDigit(state, 0);
                CalcEngine.pushOperation(state, 'add');
                CalcEngine.pushDigit(state, 5);
                CalcEngine.go(state);
                assert(CalcEngine.effectiveDisplay(state) === "15");
            }
        },
        {
            name: "10 * 5 = -> 50",
            run: function() {
                var state = CalcEngine.init();
                CalcEngine.pushDigit(state, 1);
                CalcEngine.pushDigit(state, 0);
                CalcEngine.pushOperation(state, 'mul');
                CalcEngine.pushDigit(state, 5);
                CalcEngine.go(state);
                assert(CalcEngine.effectiveDisplay(state) === "50");
            }
        },
        {
            name: "10 / 5 = -> 2",
            run: function() {
                var state = CalcEngine.init();
                CalcEngine.pushDigit(state, 1);
                CalcEngine.pushDigit(state, 0);
                CalcEngine.pushOperation(state, 'div');
                CalcEngine.pushDigit(state, 5);
                CalcEngine.go(state);
                assert(CalcEngine.effectiveDisplay(state) === "2");
            }
        },
        {
            name: "10 - 5 = -> 5",
            run: function() {
                var state = CalcEngine.init();
                CalcEngine.pushDigit(state, 1);
                CalcEngine.pushDigit(state, 0);
                CalcEngine.pushOperation(state, 'sub');
                CalcEngine.pushDigit(state, 5);
                CalcEngine.go(state);
                assert(CalcEngine.effectiveDisplay(state) === "5");
            }
        },
        {
            name: "2 ^ 8 = -> 256",
            run: function() {
                var state = CalcEngine.init();
                CalcEngine.pushDigit(state, 2);
                CalcEngine.pushOperation(state, 'exp');
                CalcEngine.pushDigit(state, 8);
                CalcEngine.go(state);
                assert(CalcEngine.effectiveDisplay(state) === "256");
            }
        },
        {
            name: "+ 5 = -> 5",
            run: function() {
                var state = CalcEngine.init();
                CalcEngine.pushOperation(state, 'add');
                CalcEngine.pushDigit(state, 5);
                CalcEngine.go(state);
                assert(CalcEngine.effectiveDisplay(state) === "5");
            }
        },
        {
            name: "10 + 5 = = -> 20",
            run: function() {
                var state = CalcEngine.init();
                CalcEngine.pushDigit(state, 1);
                CalcEngine.pushDigit(state, 0);
                CalcEngine.pushOperation(state, 'add');
                CalcEngine.pushDigit(state, 5);
                CalcEngine.go(state);
                CalcEngine.go(state);
                assert(CalcEngine.effectiveDisplay(state) === "20");
            }
        },
        {
            name: "10 + = 20",
            run: function() {
                var state = CalcEngine.init();
                CalcEngine.pushDigit(state, 1);
                CalcEngine.pushDigit(state, 0);
                CalcEngine.pushOperation(state, 'add');
                CalcEngine.go(state);
                assert(CalcEngine.effectiveDisplay(state) === "20");
            }
        },
        {
            name: 'clear',
            run: function() {
                var state = CalcEngine.init();
                CalcEngine.pushDigit(state, 1);
                CalcEngine.pushDigit(state, 0);
                CalcEngine.pushOperation(state, 'add');
                CalcEngine.go(state);
                assert(CalcEngine.effectiveDisplay(state) === "20");
                CalcEngine.pushDigit(state, 3);
                CalcEngine.pushDigit(state, 5);
                assert(CalcEngine.effectiveDisplay(state) === "35");
                CalcEngine.clear(state);
                assert(CalcEngine.effectiveDisplay(state) === "20");
            }
        },
        {
            name: 'reset',
            run: function() {
                var state = CalcEngine.init();
                CalcEngine.pushDigit(state, 1);
                CalcEngine.pushDigit(state, 0);
                CalcEngine.pushOperation(state, 'add');
                CalcEngine.go(state);
                assert(CalcEngine.effectiveDisplay(state) === "20");
                CalcEngine.pushDigit(state, 3);
                CalcEngine.pushDigit(state, 5);
                assert(CalcEngine.effectiveDisplay(state) === "35");
                CalcEngine.reset(state);
                assert(CalcEngine.effectiveDisplay(state) === "0");
            }
        }
    ];

    return {
        init: init,
        clear: clear,
        reset: reset,
        pushDigit: pushDigit,
        pushOperation: pushOperation,
        go: go,
        effectiveDisplay: effectiveDisplay,
        dispatchCommand: dispatchCommand,
        testCases: testCases
    };
})();

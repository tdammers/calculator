"use strict";

/**
 * A helper module with some functional-programming utilities.
 */
var F = (function(){
    /**
     * A flipped version of `map`. Roughly equivalent to Array.map.
     */
    var forEach = function(iteree, fn) {
        return map(fn, iteree);
    };

    /**
     * map, the usual FP suspect.
     */
    var map = function(fn, iteree) {
        if (typeof(iteree.map) === 'function') {
            return iteree.map(fn);
        }
        else {
            var i = 0;
            var result = [];
            for (i = 0; i < iteree.length; ++i) {
                result.push(fn(iteree[i], i, iteree));
            }
            return result;
        }
    };

    /**
     * Testable interface: unit tests for this module.
     */
    var testCases = [
        {
            name: 'map',
            run: function() {
                var items = [ 1, 5, 7 ];
                var fn = function(a) { return a + 1; }
                var expected = [ 2, 6, 8 ];
                var actual = map(fn, items);
                assert(JSON.stringify(expected) === JSON.stringify(actual));
            }
        },
        {
            name: 'map with index',
            run: function() {
                var items = [ 1, 5, 7 ];
                var fn = function(a, i) { return a + i; }
                var expected = [ 1, 6, 9 ];
                var actual = map(fn, items);
                assert(JSON.stringify(expected) === JSON.stringify(actual));
            }
        },
        {
            name: 'forEach',
            run: function() {
                var items = [ 1, 5, 7 ];
                var fn = function(a) { return a + 1; }
                var expected = [ 2, 6, 8 ];
                var actual = forEach(items, fn);
                assert(JSON.stringify(expected) === JSON.stringify(actual));
            }
        },
        {
            name: 'forEach with index',
            run: function() {
                var items = [ 1, 5, 7 ];
                var fn = function(a, i) { return a + i; }
                var expected = [ 1, 6, 9 ];
                var actual = forEach(items, fn);
                assert(JSON.stringify(expected) === JSON.stringify(actual));
            }
        }
    ];

    return {
        map: map,
        forEach: forEach,
        testCases: testCases
    };
})();
